<?php

namespace App\Http\Controllers;

use App\Models\Todo;
use Illuminate\Http\Request;

class TodoController extends Controller
{
    //

    function index(){
        $todos = Todo::all();
        return view('dashboard', ['todos' => $todos]);
    }

    function add(Request $request){
        $todo =  new Todo();
        $todo["task"] = $request->input("task");
        $todo->save();
        return redirect()->route("dashboard");
    }

    function drop($id)
    {
        Todo::destroy($id);
        return redirect()->route("dashboard");
    }

    function edit($id)
    {
        $itemtoedit =  Todo::where('id', $id)->first();
        $todos = Todo::all();
        return view('dashboard', ['todos' => $todos, 'itemtoedit' => $itemtoedit, 'Edit' => "Edit Todo"]);
    }

    function update(Request $request){
        $todo =  Todo::where('id', $request->id)->first();
        $todo["task"] = $request->input("task");
        $todo->save();
        return redirect()->route("dashboard");
    }

    function action(Request $request)
    {
//        switch ($request->input("action")) {
//            case 'todo':{
//
//            }
//        }
//
//        if($request->action=='todo'){
//            $todo = Todo::where('id', $request->id)->first();
//            if($todo->todo){
//                $todo->todo = 0;
//            }else{
//                $todo->todo = 1;
//            }
//            $todo->save();
//        }

        $todo = Todo::where('id', $request->id)->first();
        if($todo[$request['action']]){
            $todo[$request['action']] = 0;
        }else{
            $todo[$request['action']] = 1;
        }
        $todo->save();
        return $request;
    }
}
