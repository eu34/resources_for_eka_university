<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                        @if(isset($itemtoedit))
                            <div class="add">
                                <form action="{{route('updatetodo')}}" method="post">
                                    @csrf
                                    <textarea name="task">{{$itemtoedit->task}}</textarea>
                                    <input type="hidden" name="id" value="{{$itemtoedit->id}}">
                                    <button>Edit Todo</button>
                                </form>
                            </div>
                        @else
                            <div class="add">
                                <form action="{{route('addtodo')}}" method="post">
                                    @csrf
                                    <textarea name="task"></textarea>
                                    <button>Add Todo</button>
                                </form>
                            </div>
                        @endif
                   <div class="todo-table">
                        <table>
                           <thead>
                             <tr>
                                 <th>Text</th>
                                 <th>To Do</th>
                                 <th>Is Doing</th>
                                 <th>Done</th>
                                 <th>Edit</th>
                                 <th>Drop</th>
                             </tr>
                           </thead>
                           <tbody>
                            @foreach($todos as $todo)
                                <tr>
                                    <td>{{$todo->task}}</td>
                                    <td style="text-align: center">
                                        @if($todo->todo)
                                            <input type="checkbox" checked="checked" onclick="change_todo_status(event,'todo')" value="{{$todo->id}}">
                                        @else
                                            <input type="checkbox" onclick="change_todo_status(event, 'todo')" value="{{$todo->id}}">
                                        @endif
                                    </td>
                                    <td style="text-align: center">
                                        @if($todo->doing)
                                            <input type="checkbox" checked="checked" onclick="change_todo_status(event,'doing')" value="{{$todo->id}}">
                                        @else
                                            <input type="checkbox" onclick="change_todo_status(event, 'doing')" value="{{$todo->id}}">
                                        @endif
                                    </td>
                                    <td style="text-align: center">
                                        @if($todo->done)
                                            <input type="checkbox" checked="checked" onclick="change_todo_status(event,'done')" value="{{$todo->id}}">
                                        @else
                                            <input type="checkbox" onclick="change_todo_status(event, 'done')" value="{{$todo->id}}">
                                        @endif
                                    </td>
                                    <td><a href="{{route("edittodo", [$todo->id])}}">EDIT</a></td>
                                    <td><a href="{{route("droptodo", [$todo->id])}}">DROP</a></td>
                                </tr>
                            @endforeach
                           </tbody>
                           <tfoot></tfoot>
                       </table>
                   </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
