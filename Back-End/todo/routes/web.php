<?php

use App\Http\Controllers\ProfileController;
use App\Http\Controllers\TodoController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Route::get('/dashboard', function () {
//    return view('dashboard');
//})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');

    Route::get('/dashboard', [TodoController::class, 'index'])->name('dashboard');
    Route::post('/addtodo', [TodoController::class, 'add'])->name('addtodo');
    Route::get('/droptodo/{id}', [TodoController::class, 'drop'])->name('droptodo');
    Route::get('/edittodo/{id}', [TodoController::class, 'edit'])->name('edittodo');
    Route::post('/updatetodo', [TodoController::class, 'update'])->name('updatetodo');

    Route::get('/action', [TodoController::class, 'action'])->name('action');


});

require __DIR__.'/auth.php';
